package main

import (
	"log"
	"time"
	"wat-api/handlers"
	"wat-api/hash"
	"wat-api/models"
	"wat-api/mw"

	"wat-api/config"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open(
		"mysql",
		conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true)

	if err := db.AutoMigrate(
		&models.User{},
		&models.Event{},
		&models.Temple{},
		&models.Milestone{},
	).Error; err != nil {
		log.Fatal(err)
	}
	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	hmacKey := "secret"
	hmac := hash.NewHMAC(hmacKey)

	//Services
	us := models.NewUserService(db, hmac)
	es := models.NewEventService(db)
	ts := models.NewTempleService(db)
	ms := models.NewMilestoneService(db)
	// gs := models.NewGeolocationService(db)

	//Handlers
	uh := handlers.NewUserhandlers(us)
	eh := handlers.NewEventhandlers(es)
	th := handlers.NewTempleHandlers(ts)
	// th := handlers.NewTempleHandlers(ts, gs)
	mh := handlers.NewMilestoneHandler(ms)
	// gh := handlers.NewGeolocationHandlerHandler(gs)

	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	r.GET("/sessions", func(c *gin.Context) {
		user, ok := c.Value("user").(*models.User)
		if !ok {
			c.JSON(401, gin.H{
				"message": "invalid token",
			})
			return
		}
		c.JSON(200, user)
	})

	//Route
	r.POST("/signup", uh.Singup)
	r.POST("/login", uh.Login)

	r.GET("/temples", th.ListTemple) // ลิส pub วัด

	r.GET("/temples/:id", th.GetOneTemple)
	r.GET("temples/:id/milestones", mh.ListMilestone)

	r.GET("/events", eh.ListEvent)

	r.GET("/user/:id", uh.GetUserByID)

	protect := r.Group("/protected")
	protect.Use(mw.RequireUser(us))
	{
		protect.POST("/temples", th.CreateTemple) //สร้างวัด
		protect.GET("/temples", th.ListUserTemple)
		protect.POST("temples/:id/milestones", mh.CreateMilestone)

		protect.POST("/events/:id", eh.CreateEvent)
		protect.GET("/events", eh.ListUserEvent)
		protect.DELETE("/events/:id", eh.DeleteEvent)

		protect.PUT("/temples/:id/image", th.UpdateImage)
	}

	r.Run()
}
