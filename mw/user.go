package mw

import (
	"log"
	"strings"
	"wat-api/models"

	"github.com/gin-gonic/gin"
)

func RequireUser(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Print("Check called ------------------------------->")
		header := c.GetHeader("Authorization")

		header = strings.TrimSpace(header)
		if len(header) <= 7 {
			log.Print("SHORT HEADER ------------------------------->")
			c.Status(401)
			c.Abort()
			return
		}
		token := header[len("Bearer "):]

		if token == "" {

			c.Status(400)
			c.Abort()
			return
		}
		user, err := us.GetbyToken(token)
		if err != nil {
			log.Print("INCORRECT TOKEN ------------------------------->")
			c.Status(401)
			c.Abort()
			return
		}
		log.Print("Setting user ------------------------------->")
		c.Set("user", user)
	}
}
