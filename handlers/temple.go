package handlers

import (
	"strconv"
	"wat-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type TempleHandlers struct {
	ts models.TempleService
}

func NewTempleHandlers(ts models.TempleService) *TempleHandlers {
	return &TempleHandlers{ts}
}

type CreateTemple struct {
	gorm.Model
	Name      string
	OpCl      string
	Location  string
	Tel       string
	Website   string
	Type      string
	District  string
	UserID    uint
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
	Image     string `json:"image"`
}

type ListTemple struct {
	ID        uint   `json:"id"`
	Name      string `json:"name"`
	OpCl      string `json:"opcl"`
	Location  string `json:"location"`
	Tel       string `json:"tel"`
	Website   string `json:"website"`
	Type      string `json:"type"`
	District  string `json:"district"`
	UserID    uint
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
	Image     string `json:"image"`
}

type CreateTempleRes struct {
	ListTemple
}

func (th *TempleHandlers) CreateTemple(c *gin.Context) {

	user, ok := c.Value("user").(models.User)

	if !ok {
		c.Status(401)
		return
	}

	data := new(CreateTemple)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	temple := new(models.Temple)
	temple.Name = data.Name
	temple.Location = data.Location
	temple.Tel = data.Tel
	temple.Website = data.Website
	temple.Type = data.Type
	temple.OpCl = data.OpCl
	temple.District = data.District
	temple.Latitude = data.Latitude
	temple.Longitude = data.Longitude
	temple.Image = data.Image
	temple.UserID = user.ID
	err := th.ts.Create(temple)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	res := new(CreateTempleRes)
	res.ID = temple.ID
	res.Name = temple.Name
	res.Location = temple.Location
	res.OpCl = temple.OpCl
	res.Tel = temple.Tel
	res.Type = temple.Type
	res.Website = temple.Website
	res.District = temple.District
	res.Latitude = temple.Latitude
	res.Longitude = temple.Longitude
	res.Image = temple.Image
	res.UserID = temple.UserID

	c.JSON(201, res)
}

func (th *TempleHandlers) ListTemple(c *gin.Context) {
	listTemple, err := th.ts.ListTemple()
	if err != nil {
		c.JSON(500, gin.H{
			"listTemple message": err.Error(),
		})
	}
	c.JSON(200, listTemple)
}

func (th *TempleHandlers) ListUserTemple(c *gin.Context) {

	user, ok := c.Value("user").(models.User)
	if !ok {
		c.Status(401)
		return
	}

	data, err := th.ts.ListUserTemple(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error,
		})
		return
	}
	// temples := []ListTemple{}
	// for _, d := range data {
	// 	temples = append(temples, ListTemple{
	// 		ID:       d.ID,
	// 		Name:     d.Name,
	// 		OpCl:     d.OpCl,
	// 		Location: d.Location,
	// 		Tel:      d.Tel,
	// 		Website:  d.Website,
	// 		Type:     d.Type,
	// 	})
	// }
	c.JSON(200, data)
}

func (th *TempleHandlers) GetOneTemple(c *gin.Context) {

	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := th.ts.GetOneTemple(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, ListTemple{
		ID:       data.ID,
		Name:     data.Name,
		OpCl:     data.OpCl,
		Location: data.Location,
		Tel:      data.Tel,
		Website:  data.Website,
		Type:     data.Type,
	})
}

type UpdateImageReq struct {
	Image string `json:"image"`
}

func (th *TempleHandlers) UpdateImage(c *gin.Context) {
	user, ok := c.Value("user").(models.User)
	if !ok {
		c.Status(401)
		return
	}
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, err)
		return
	}
	req := new(UpdateImageReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, err)
	}
	err = th.ts.UpdateImage(uint(id), req.Image)
	if err != nil {
		c.JSON(400, err)
		return
	}
	c.JSON(200, gin.H{
		"id":    user.ID,
		"image": req.Image,
	})
}
