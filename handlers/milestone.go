package handlers

import (
	"strconv"
	"time"
	"wat-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type MilestoneHandler struct {
	ms models.MilestoneService
}

func NewMilestoneHandler(ms models.MilestoneService) *MilestoneHandler {
	return &MilestoneHandler{ms}
}

type Milestone struct {
	gorm.Model
	EventName   string
	Description string
	Date        time.Time
	UserID      uint
	TempleID    uint
}

type ListMilestone struct {
	ID          uint      `json:"id"`
	EventName   string    `json:"eventname"`
	Description string    `json:"description"`
	Date        time.Time `json:"date"`
	UserID      uint
	TempleID    uint
}

type CreateMilestoneRes struct {
	ListMilestone
}

func (mh *MilestoneHandler) CreateMilestone(c *gin.Context) {

	user, ok := c.Value("user").(models.User)
	if !ok {
		c.Status(401)
		return
	}

	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	data := new(Milestone)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	milestone := new(models.Milestone)
	milestone.EventName = data.EventName
	milestone.Description = data.Description
	milestone.Date = data.Date
	milestone.UserID = user.ID
	milestone.TempleID = uint(id)
	err = mh.ms.Create(milestone)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(CreateMilestoneRes)
	res.ID = milestone.ID
	res.EventName = milestone.EventName
	res.Description = milestone.Description
	res.Date = milestone.Date
	res.UserID = milestone.UserID
	res.TempleID = milestone.TempleID

	c.JSON(201, res)
}

func (mh *MilestoneHandler) ListMilestone(c *gin.Context) {

	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	listMilestone, err := mh.ms.ListMilestone(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"listMilestone message": err.Error(),
		})
		return
	}
	milestones := []ListMilestone{}
	for _, d := range listMilestone {
		milestones = append(milestones, ListMilestone{
			ID:          d.ID,
			EventName:   d.EventName,
			Description: d.Description,
			Date:        d.Date,
			UserID:      d.UserID,
			TempleID:    d.TempleID,
		})
	}
	// res := new(CreateMilestoneRes)
	// res.ID = listMilestone.ID
	// res.EventName = milestone.EventName
	// res.Description = milestone.Description
	// res.Date = milestone.Date
	// res.UserID = milestone.UserID
	// res.TempleID = milestone.TempleID

	c.JSON(200, milestones)

}
