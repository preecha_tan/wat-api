package handlers

import (
	"strconv"
	"wat-api/models"

	"github.com/gin-gonic/gin"
)

type Userhandlers struct {
	us models.UserService
}

func NewUserhandlers(us models.UserService) *Userhandlers {
	return &Userhandlers{us}
}

type SingupReq struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Role      bool   `json:"role"`
}

type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Token    string `gorm:"unique_index"`
}

type UserByID struct {
	ID   uint `json:"id"`
	Role bool `json:"role"`
}

func (uh *Userhandlers) Singup(c *gin.Context) {
	req := new(SingupReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{ //
			"message": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	user.Firstname = req.Firstname
	user.Lastname = req.Lastname
	user.Role = false
	if err := uh.us.Create(user); err != nil {
		c.JSON(400, gin.H{ //
			"message": err.Error(),
		})
		c.JSON(201, gin.H{
			"token":     user.Token,
			"email":     user.Email,
			"firstname": user.Firstname,
			"lastname":  user.Lastname,
			"role":      user.Role,
		})
	}
}

func (uh *Userhandlers) Login(c *gin.Context) {
	req := new(LoginReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{ //
			"message": err.Error(),
		})
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password

	var token string
	var err error
	if token, err = uh.us.Login(user); err != nil {
		c.JSON(401, gin.H{ //
			"message": err.Error(),
		})
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (uh *Userhandlers) GetUserByID(c *gin.Context) {

	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	data, err := uh.us.GetUserByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
