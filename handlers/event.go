package handlers

import (
	"log"
	"strconv"
	"time"
	"wat-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Eventhandlers struct {
	es models.EventService
}

func NewEventhandlers(es models.EventService) *Eventhandlers {
	return &Eventhandlers{es}
}

type CreateEvent struct {
	gorm.Model
	EventName   string    `json:"eventname"`
	Description string    `json:"description"`
	Date        time.Time `json:"date"`
	UserID      uint
}

type ListEvent struct {
	ID          uint      `json:"id"`
	EventName   string    `json:"eventname"`
	Description string    `json:"description"`
	Date        time.Time `json:"date"`
	UserID      uint
}

type CreateRes struct {
	ListEvent
}

func (eh *Eventhandlers) CreateEvent(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	user, ok := c.Value("user").(models.User)
	// user := context.User(c)
	log.Printf("User %+v", user)
	if !ok {
		c.Status(401)
		return
	}

	data := new(CreateEvent)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	event := new(models.Event)
	event.EventName = data.EventName
	event.Description = data.Description
	event.Date = data.Date
	event.UserID = user.ID
	event.TempleID = uint(id)
	err = eh.es.Create(event)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	res := new(CreateRes)
	res.ID = event.ID
	res.EventName = event.EventName
	res.Description = event.Description
	res.Date = event.Date
	res.UserID = event.UserID
	// c.JSON(201, gin.H{
	// 	"id":   gallery.ID,
	// 	"name": gallery.Name,
	// })
	c.JSON(201, res)
}

func (eh *Eventhandlers) ListEvent(c *gin.Context) {
	listEvent, err := eh.es.ListEvent()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(200, listEvent)
}

func (eh *Eventhandlers) ListUserEvent(c *gin.Context) {

	user, ok := c.Value("user").(models.User)
	log.Printf("User %+v", user)
	if !ok {
		c.Status(401)
		return
	}

	data, err := eh.es.ListUserEvent(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error,
		})
		return
	}
	events := []ListEvent{}
	for _, d := range data {
		events = append(events, ListEvent{
			ID:          d.ID,
			EventName:   d.EventName,
			Description: d.Description,
			Date:        d.Date,
		})
	}
	c.JSON(200, events)
}

func (eh *Eventhandlers) DeleteEvent(c *gin.Context) {

	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if err := eh.es.DeleteEvent(uint(id)); err != nil {
		log.Println(err)
		c.JSON(500, gin.H{ //500 error
			"message": err.Error(),
		})
	}
	c.Status(204)
}
