package handlers

import "wat-api/models"

type GeolocationHandler struct {
	gs models.GeolocationService
}

func NewGeolocationHandlerHandler(gs models.GeolocationService) *GeolocationHandler {
	return &GeolocationHandler{gs}
}

type Geolocation struct {
	Longtitude string `gorm:"not null"`
	Latitude   string `gorm:"not null"`
	PubID      uint   `gorm:"not null"`
}
