package models

import "github.com/jinzhu/gorm"

type TempleService interface {
	Create(temple *Temple) error
	ListTemple() ([]Temple, error)
	GetOneTemple(id uint) (*Temple, error)
	ListUserTemple(id uint) ([]Temple, error)
	UpdateImage(id uint, image string) error
}

func NewTempleService(db *gorm.DB) TempleService {
	return &templeGorm{db}
}

type templeGorm struct {
	db *gorm.DB
}

type Temple struct {
	gorm.Model
	Name      string `json:"name"`
	OpCl      string `json:"opcl"`
	Location  string `json:"location"`
	Tel       string `json:"tel"`
	Website   string `json:"website"`
	Type      string `json:"type"`
	District  string `json:"district"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
	Image     string `json:"image"`
	UserID    uint
}

func (tg *templeGorm) Create(temple *Temple) error {
	return tg.db.Create(temple).Error
}

func (tg *templeGorm) ListTemple() ([]Temple, error) {
	temples := []Temple{}
	if err := tg.db.Find(&temples).Error; err != nil {
		return nil, err
	}
	return temples, nil
}

func (tg *templeGorm) ListUserTemple(id uint) ([]Temple, error) {
	temples := []Temple{}
	if err := tg.db.
		Where("user_id = ?", id).
		Find(&temples).Error; err != nil {
		return nil, err
	}
	return temples, nil
}

func (tg *templeGorm) GetOneTemple(id uint) (*Temple, error) {
	temple := new(Temple)
	if err := tg.db.First(temple, id).Error; err != nil {
		return nil, err
	}
	return temple, nil
}
func (tg *templeGorm) UpdateImage(id uint, image string) error {
	return tg.db.Model(&Temple{}).Where("id = ?", id).
		Update("image", image).Error
}
