package models

import "github.com/jinzhu/gorm"

type Geolocation struct {
	Longtitude string `gorm:"not null"`
	Latitude   string `gorm:"not null"`
	TempleID   uint   `gorm:"not null"`
}

type GeolocationService interface {
	Create(geolocation *Geolocation) error
	// Login(user *User) (string, error)
	// GetByToken(token string) (*User, error)
	// Logout(user *User) error
	// GetByID(id uint) (*User, error)
	// UpdateProfile(id uint, name string) error
	// CheckUsername(username string) bool
}

func NewGeolocationService(db *gorm.DB) GeolocationService {
	return &geolocationGorm{db}
}

type geolocationGorm struct {
	db *gorm.DB
}

func (gg *geolocationGorm) Create(geolocation *Geolocation) error {
	return gg.db.Create(geolocation).Error
}
