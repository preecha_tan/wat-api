package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Milestone struct {
	gorm.Model
	EventName   string
	Description string
	Date        time.Time
	UserID      uint
	TempleID    uint
}

type MilestoneService interface {
	Create(milstone *Milestone) error
	ListMilestone(id uint) ([]Milestone, error)
	GetOneMilestone(id uint) ([]Milestone, error)
}

func NewMilestoneService(db *gorm.DB) MilestoneService {
	return &milestoneGorm{db}
}

type milestoneGorm struct {
	db *gorm.DB
}

func (mg *milestoneGorm) Create(milstone *Milestone) error {
	return mg.db.Create(milstone).Error
}

func (mg *milestoneGorm) GetOneMilestone(id uint) ([]Milestone, error) {
	milestones := []Milestone{}
	if err := mg.db.
		Where("temple_id= ?", id).
		Find(&milestones).Error; err != nil {
		return nil, err
	}
	return milestones, nil
}

func (mg *milestoneGorm) ListMilestone(id uint) ([]Milestone, error) {
	milestones := []Milestone{}
	if err := mg.db.
		Where("temple_id = ?", id).
		Find(&milestones).Error; err != nil {
		return nil, err
	}
	return milestones, nil
}
