package models

import (
	"fmt"
	"wat-api/hash"
	"wat-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const cost = 12
const key = "secret"

type userGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	// mac := hmac.New(sha256.New, []byte(hmacKey))
	return &userGorm{db, hmac}
}

type UserService interface {
	Create(user *User) error
	Login(user *User) (string, error)
	GetbyToken(token string) (User, error)
	GetUserByID(id uint) (*User, error)
}

type User struct {
	gorm.Model
	Email     string `gorm:"unique_index;not null" json:"email"`
	Password  string `gorm:"not null"`
	Token     string `gorm:"unique_index"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Role      bool   `json:"role"`
}

func (ug *userGorm) Create(user *User) error {

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.Gettoken()
	if err != nil {
		return err
	}
	user.Token = token
	return ug.db.Create(user).Error
}

func (ug *userGorm) Login(user *User) (string, error) {
	found := new(User)
	err := ug.db.Where("email =?", user.Email).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := rand.Gettoken()
	if err != nil {
		return "", err
	}
	tokenHash := ug.hmac.Hash(token)
	err = ug.db.Model(&User{}).
		Where("id = ?", found.ID).
		Update("token", tokenHash).Error
	if err != nil {
		return "", err
	}
	return token, nil

}

func (ug *userGorm) GetbyToken(token string) (User, error) {
	tokenHash := ug.hmac.Hash(token)
	fmt.Println("hash", tokenHash)
	var user User
	if err := ug.db.Where("token = ?", tokenHash).First(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

func (ug *userGorm) GetUserByID(id uint) (*User, error) {
	userbyid := User{}
	err := ug.db.Where("id = ?", id).Find(&userbyid).Error
	if err != nil {
		return nil, err
	}
	return &userbyid, nil
}
