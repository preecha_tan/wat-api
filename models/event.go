package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type EventService interface {
	Create(event *Event) error
	ListEvent() ([]Event, error)
	ListUserEvent(id uint) ([]Event, error)
	DeleteEvent(id uint) error
}

func NewEventService(db *gorm.DB) EventService {
	return &eventGorm{db}
}

type eventGorm struct {
	db *gorm.DB
}

type Event struct {
	gorm.Model
	EventName   string    `json:"eventname"`
	Description string    `json:"description"`
	Date        time.Time `json:"date"`
	UserID      uint      `json:"userid"`
	TempleID    uint      `json:"templeid"`
}

func (eg *eventGorm) Create(event *Event) error {
	return eg.db.Create(event).Error
}

func (eg *eventGorm) ListEvent() ([]Event, error) {
	events := []Event{}
	if err := eg.db.Find(&events).Error; err != nil {
		return nil, err
	}
	return events, nil
}

func (eg *eventGorm) ListUserEvent(id uint) ([]Event, error) {
	fmt.Printf("IDdddddddddddddddddddddddd %v", id)
	events := []Event{}
	if err := eg.db.
		Where("user_id = ?", id).
		Find(&events).Error; err != nil {
		return nil, err
	}
	fmt.Printf("Eventssssssssssssssssssss %v", events)
	return events, nil
}

func (eg *eventGorm) DeleteEvent(id uint) error {
	tx := eg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		return err
	}

	err := eg.db.Where("id = ?", id).Delete(&Event{}).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
